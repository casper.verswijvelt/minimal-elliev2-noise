package be.basalte.noise

import android.content.Intent
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.LinkProperties
import android.net.LocalSocket
import android.net.LocalSocketAddress
import android.net.Network
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.zykronix.audiocodecctl.AudioCodecCtl
import org.linphone.core.Address
import org.linphone.core.Call
import org.linphone.core.Core
import org.linphone.core.CoreListenerStub
import org.linphone.core.Factory
import org.linphone.core.MediaDirection
import org.linphone.mediastream.video.capture.CaptureTextureView
import java.io.IOException


const val TAG = "QR"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val txtCallState = TextView(this).apply { textSize = 20f }
        val txtIpAddress = TextView(this).apply { textSize = 20f }
        val txtQR = TextView(this).apply { textSize = 20f }

        window.attributes = window.attributes.apply {
            screenBrightness = 1f
        }
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        // Linphone
        Factory.instance().setDebugMode(true, TAG);
        val linphoneCore: Core = try {
            Factory.instance().createCore(null, null, this).apply {

                // Make reachable via IP address on port 5060
                transports = transports.apply {
                    udpPort = 5060
                }

                inCallTimeout = Int.MAX_VALUE
                incTimeout = Int.MAX_VALUE
                nortpTimeout = Int.MAX_VALUE

                setQrcodeDecodeRect(0, 0, 720, 1280)
                isQrcodeVideoPreviewEnabled = true

                isVideoCaptureEnabled = true
                isVideoPreviewEnabled = true

                videoDevice = videoDevicesList.find {
                    it == "BackFacingCamera"
                }

                addListener(object : CoreListenerStub() {

                    override fun onCallStateChanged(
                        linphoneCore: Core,
                        linphoneCall: Call,
                        state: Call.State?,
                        message: String
                    ) {
                        txtCallState.text = "Call state: $message"
                    }

                    override fun onQrcodeFound(core: Core, result: String?) {
                        txtQR.text = "QR: $result"
                        isVideoPreviewEnabled = false
                    }
                })

                start()
            }
        } catch (e: IllegalArgumentException) {
            Log.e(TAG, "Failed to start core: $e")
            return
        }

        fun restartCapture() {
            linphoneCore.isVideoPreviewEnabled = false
            Handler(Looper.getMainLooper()).postDelayed({
                linphoneCore.isVideoPreviewEnabled = true
            }, 50)
        }

        setContentView(LinearLayout(this).apply {
            orientation = LinearLayout.VERTICAL

            addView(txtCallState)
            addView(txtIpAddress)
            addView(txtQR)
            addView(Button(this@MainActivity).apply {
                text = "ACCEPT"
                setOnClickListener {
                    linphoneCore.currentCall?.acceptWithParams(linphoneCore.createCallParams(linphoneCore.currentCall)?.apply {
                        isVideoEnabled = true
                        isAudioEnabled = true
                        audioDirection = MediaDirection.SendRecv
                        videoDirection = MediaDirection.SendRecv
                    })
                }
            })
            addView(Button(this@MainActivity).apply {
                text = "TERMINATE"
                setOnClickListener {
                    linphoneCore.currentCall?.terminate()
                }
            })
            addView(Button(this@MainActivity).apply {
                text = "INVITE"
                setOnClickListener {
                    val params = linphoneCore.createCallParams(null)?.apply {
                        videoDirection = MediaDirection.SendRecv
                        audioDirection = MediaDirection.SendRecv
                        isAudioEnabled = true
                        isVideoEnabled = true
                    } ?: return@setOnClickListener
                    val address = linphoneCore.createAddress("sip:172.147.20.15") ?: return@setOnClickListener

                    linphoneCore.inviteAddressWithParams(address, params)
                }
            })
            addView(Button(this@MainActivity).apply {
                text = "RESTART CAPTURE"
                setOnClickListener {
                    restartCapture()
                }
            })
            addView(CaptureTextureView(this@MainActivity).apply {
                linphoneCore.nativePreviewWindowId = this
                layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1500)
            })
        })

        // Display network address
        (getSystemService(CONNECTIVITY_SERVICE) as? ConnectivityManager)?.apply {
            registerDefaultNetworkCallback(object : NetworkCallback() {

                override fun onLinkPropertiesChanged(
                    network: Network,
                    linkProperties: LinkProperties
                ) {
                    txtIpAddress.text =
                        linkProperties.linkAddresses.filter { it.prefixLength == 16 || it.prefixLength == 24 }
                            .joinToString("\n") { "sip:${it.address.toString().drop(1)}" }
                }
            })
        }

        // Request microphone permission
        requestPermissions(arrayOf(android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA), 9820)
    }
}
